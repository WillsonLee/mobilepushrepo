<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','userController@index');
Route::get('/gettoken','userController@gettoken');
// Route::get('/gettoken', array('middleware' => 'cors', 'uses' => 'userController@gettoken'));
// Route::post('/addtoken', array('middleware' => 'cors', 'uses' => 'userController@addtoken'));
Route::post('/addtoken','userController@addtoken');
Route::post('/pushmsg','userController@sendPush')->name('send-push');
Route::post('/deldevice','userController@sendDelete')->name('send-delete');
Route::post('/pushmulmsg','userController@sendmulti')->name('send-multi-push');
