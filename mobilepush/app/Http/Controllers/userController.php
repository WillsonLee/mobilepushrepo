<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use \App\User;

class userController extends Controller
{
    protected $serverKey;

    public function __construct()
    {
        $this->serverKey = config('app.firebase_server_key');
    }

    public function index()
    {
        $users = User::all();
        return view('welcome')->with('users',$users);
    }

    public function gettoken()
    {
        return csrf_token();
    }

    public function addtoken (Request $request)
    {

        $userlist = User::where('name', $request->name)->get();
        $userCount = $userlist->count();

        if($userCount == 0){
            //$user = User::find($request->user_id);
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->name;
            $user->password = '1';
            $user->device_token = $request->device_token;
            $user->save();

            if($user)
                return response()->json([
                    'message' => 'New User token inserted'
                ]);

            return response()->json([
                'message' => 'Error!'
            ]);
        }else{
            User::where('name', $request->name)->update([
                'device_token' => $request->device_token,
            ]);
            return response()->json([
                'message' => 'User token updated'
            ]);
        }
    }

    public function sendPush2 (Request $request)
    {
        try{

        //$user = User::find($request->id);
        // $data = [
        //     "to" => $request->device_token,
        //     "data" =>
        //         [
        //             "title" => 'Web Push',
        //             "body" => "Sample Notification",
        //             "icon" => url('/logo.png')
        //         ],
        // ];
        // $dataString = json_encode($data);

        // $data = [
        //     "to" => $request->device_token,
        //     "title" => 'Web Push',
        //     "body" => "Sample Notification",
        // ];

        $response                      = array();
        $response['data']['title']     = "title test";
        $response['data']['body']   = "body message";
        $response['data']['timestamp'] = date( 'Y-m-d G:i:s' );

        // $fields = array(
        //     'to'   => $request->device_token,
        //     'data' => $response,
        //     );

        $fields = array(
            'to'   => $request->device_token,
            'data' => $response,
            "title" => 'Web Push',
            "body" => "Sample Notification",
            );

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        //$client = new Client();
        $client = new Client();

        // $result = $client->post( $url, [
        //    'json'    =>
        //       $fields
        //    ,
        //    'headers' => [
        //         'Authorization' => 'key='.env('FIREBASE_SERVER_KEY'),
        //         'Content-Type'  => 'application/json',
        //    ],
        // ] );

        $result = $client->request('POST',$url,
            [
                'json' =>
                // [
                //     "to" => $request->device_token,
                //     'id' => 3,
                //     'title' => 'a1',
                //     'authorization' => '55ca79075a609779071c3c040e6f9706',
                //     'push_type' => 2,
                //     'content' => 'aaaa',
                // ],
                [
                    "to" => $request->device_token,
                    "data" =>
                    [
                        "title" => 'Web Push',
                        "body" => "Sample Notification",
                        "icon" => url('/logo.png')
                    ],
                ],
                'headers' => [
                     'Authorization' => 'key='.env('FIREBASE_SERVER_KEY'),
                     'Content-Type'  => 'application/json',
                     'debug' => true
                ],
            ]
        );

        //return json_decode( $result->getBody(), true );
        //return $dataString;
        return redirect('/')->with('message', 'Notification sent!');


        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }

    public function gettok()
    {
        $users = User::all();

        foreach($users as $user)
        {
            $alltokens[] = $user->device_token;
        }
        return $alltokens;
    }

    public function sendpush(Request $request)
    {

        if($request->input('selimg') != null){
            $upimg = $request->file('uploadimg');
            //$input['imagename'] = time() . '-' . $request->getClientOriginalName() . '.' . $upimg->getClientOriginalExtension();
            $input['imagename'] = date('Y-m-d H:i:s') . '-' .  $upimg->getClientOriginalName();
            $destinationPath = public_path('/banner');
            $upimg->move($destinationPath,$input['imagename']);
        }

        if($request->input('selurl') != null){
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
                "image" => $request->imgurl,
            ];
        }
        else if($request->input('selimg') != null){
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
                "image" => url('/banner' . '/' . $input['imagename']),
            ];
        }else{
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
            ];
        }

        try{
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        //$client = new Client();
        $client = new Client();

        $result = $client->request('POST',$url,
            [
                'json' =>
                [
                    "to" => $request->device_token,
                    "collapse_key" => "type_a",
                    "notification" => $optionsimg,
                ],
                'headers' => [
                     'Authorization' => 'key='.env('FIREBASE_SERVER_KEY'),
                     'Content-Type'  => 'application/json',
                     'debug' => true
                ],
            ]
        );
        return redirect('/')->with('message', 'Notification sent!');

        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }
    public function sendDelete(Request $request)
    {
        try{

            $user = User::find($request->id);

            $user->delete();

            return redirect('/')->with('message', 'device deleted!');

        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }


    public function sendmulti(Request $request)
    {

        if($request->input('selimg') != null){
            $upimg = $request->file('uploadimg');
            //$input['imagename'] = time() . '-' . $request->getClientOriginalName() . '.' . $upimg->getClientOriginalExtension();
            $input['imagename'] = date('Y-m-d H:i:s') . '-' .  $upimg->getClientOriginalName();
            $destinationPath = public_path('/banner');
            $upimg->move($destinationPath,$input['imagename']);
        }

        if($request->input('selurl') != null){
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
                "image" => $request->imgurl,
            ];
        }
        else if($request->input('selimg') != null){
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
                "image" => url('/banner' . '/' . $input['imagename']),
            ];
        }else{
            $optionsimg = [
                "title" => $request->title,
                "body" => $request->message,
            ];
        }

        try{

            //get all device token
            $users = User::all();

            // Set POST variables
            $url = 'https://fcm.googleapis.com/fcm/send';

            foreach($users as $user)
            {
                //$alltokens[] = $user->device_token;
                //$client = new Client();
                $client = new Client();

                $result = $client->request('POST',$url,
                    [
                        'json' =>
                        [
                            "to" => $user->device_token,
                            "collapse_key" => "type_a",
                            "notification" => $optionsimg,
                        ],
                        'headers' => [
                            'Authorization' => 'key='.env('FIREBASE_SERVER_KEY'),
                            'Content-Type'  => 'application/json',
                            'debug' => true
                        ],
                    ]
                );
            }
            return redirect('/')->with('message', 'Notification sent!');

        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }

    public function sendmultiori(Request $request)
    {
        try{

        //get all device token
        $users = User::all();

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        foreach($users as $user)
        {
            //$alltokens[] = $user->device_token;
            //$client = new Client();
            $client = new Client();

            $result = $client->request('POST',$url,
                [
                    'json' =>
                    [
                        "to" => $user->device_token,
                        "collapse_key" => "type_a",
                        "notification" =>
                        [
                            "title" => $request->title,
                            "body" => $request->message,
                            "imgurl" => $request->imgurl,
                        ]
                        // ,
                        // "data" =>
                        // [
                        //     "title" => $request->title,
                        //     "body" => $request->message,
                        // ],
                    ],
                    'headers' => [
                        'Authorization' => 'key='.env('FIREBASE_SERVER_KEY'),
                        'Content-Type'  => 'application/json',
                        'debug' => true
                    ],
                ]
            );
        }
        //return $alltokens;



        return redirect('/')->with('message', 'Notification sent!');

        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }


}
