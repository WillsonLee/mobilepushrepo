<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

            <link rel="stylesheet" href="{{asset('css/plugins/dataTables/datatables.min.css')}}">
        <!-- Styles -->
        <style>
            table {
            width: 750px;
            border-collapse: collapse;
            margin:50px auto;
            }

            /* Zebra striping */
            tr:nth-of-type(odd) {
                background: #eee;
                }

            th {
                background: #3498db;
                color: white;
                font-weight: bold;
                }

            td, th {
                padding: 10px;
                border: 1px solid #ccc;
                text-align: left;
                font-size: 18px;
                }

            /*
            Max width before this PARTICULAR table gets nasty
            This query will take effect for any screen smaller than 760px
            and also iPads specifically.
            */
            @media
            only screen and (max-width: 760px),
            (min-device-width: 768px) and (max-device-width: 1024px)  {

            table {
                width: 100%;
            }

            /* Force table to not be like tables anymore */
            table, thead, tbody, th, td, tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr { border: 1px solid #ccc; }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
                /* Label the data */
                content: attr(data-column);

                color: #000;
                font-weight: bold;
            }

        }
        </style>

        <script>
            function ckselurl() {
                document.getElementsByName("image").disabled = false;
                document.getElementsByName("uploadimg").disabled = true;
                var ckName = document.getElementById("selimg").checked = false;
            }

            function ckselimg() {
                document.getElementsByName("uploadimg").disabled = false;
                document.getElementsByName("image").disabled = true;
                var ckName = document.getElementById("selurl").checked = false;
            }
        </script>

    </head>
        <div id="app" class="container">
                <br>
                <div class="col-md-8">
                    <div class="card">
                        <h1 class="card-header" style="text-align: center;">Send push to Users</h1>

                        <body>
                            @if(Session::has('message'))
                                <div class="alert alert-success"  style="text-align: center;">
                                    {{session('message')}}
                                </div>
                            @endif
                        <br>
                        <div style="text-align: center;">
                            <form action="{{ route('send-multi-push') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="title">title:</label>
                                    <input name="title" value="sample multi title"  type="title" class="form-control" id="title">
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="message">message:</label>
                                    <input name="message" value="sample multi message"  type="message" class="form-control" id="message">
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="checkbox" id="selurl" name="selurl[]" value="selurlvalue" onClick="ckselurl()">
                                    <label>Image URL:</label>
                                    <input name="imgurl" value="https://upload.wikimedia.org/wikipedia/commons/c/cb/Bee_on_Yellow_Flower.jpg" />
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="checkbox" id="selimg" name="selimg[]" value="selimgvalue" onClick="ckselimg()">
                                    <label>Image Upload:</label>
                                    <input type="file" name="uploadimg" id="uploadimg" />
                                </div>
                                <br>
                                <button type="submit" class="btn btn-default">send to all</button>
                              </form>

                        </div>
                        <div class="card-body">

                            <table class="table-headers">
                                <thead>
                                    <tr>
                                    <th scope="col">uuid</th>
                                    <th scope="col">token</th>
                                    <th scope="col">Action</th>
                                    <th scope="col">delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->device_token }}</td>
                                            <td>
                                                <form action="{{ route('send-push') }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" name="device_token" value="{{$user->device_token}}" />
                                                    <div>
                                                        <label>title:</label>
                                                        <input name="title" value="sample title" />
                                                    </div>
                                                    <div>
                                                        <label>message:</label>
                                                        <input name="message" value="sample message" />
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" id="selurl" name="selurl[]" value="selurlvalue" onClick="ckselurl()">
                                                        <label>Image URL:</label>
                                                        <input name="imgurl" value="https://upload.wikimedia.org/wikipedia/commons/c/cb/Bee_on_Yellow_Flower.jpg" />
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" id="selimg" name="selimg[]" value="selimgvalue" onClick="ckselimg()">
                                                        <label>Image Upload:</label>
                                                        <input type="file" name="uploadimg" id="uploadimg" />
                                                    </div>
                                                    <input class="btn btn-primary" type="submit" value="Send Push">
                                                </form>
                                            </td>
                                            <td>
                                                <form action="{{ route('send-delete') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$user->id}}" />
                                                    <input class="btn btn-primary" type="submit" value="delete">
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    </body>
</html>
